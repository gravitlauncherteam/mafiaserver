package pro.gravit.mafia.server;

import pro.gravit.launcher.Launcher;
import pro.gravit.launcher.config.JsonConfigurable;
import pro.gravit.mafia.server.commands.NextStateCommand;
import pro.gravit.mafia.server.commands.StopCommand;
import pro.gravit.mafia.server.socket.NettyConfig;
import pro.gravit.mafia.server.socket.WebSocketService;
import pro.gravit.mafia.server.socket.handlers.NettyServerHandler;
import pro.gravit.utils.command.CommandHandler;
import pro.gravit.utils.command.JLineCommandHandler;
import pro.gravit.utils.command.StdCommandHandler;
import pro.gravit.utils.command.basic.ClearCommand;
import pro.gravit.utils.command.basic.GCCommand;
import pro.gravit.utils.command.basic.HelpCommand;
import pro.gravit.utils.helper.CommonHelper;
import pro.gravit.utils.helper.LogHelper;

import java.nio.file.Paths;

public class Main {
    public static NettyServerHandler server;
    public static MafiaConfig config;
    public static CommandHandler commandHandler;
    public static void main(String[] args) throws Exception
    {
        LogHelper.printVersion("MafiaServer");
        LogHelper.printLicense("MafiaServer");
        Launcher.gsonManager = new MafiaGsonManager();
        Launcher.gsonManager.initGson();
        JsonConfigurable<MafiaConfig> mafiaConfigurable = new JsonConfigurable<MafiaConfig>(MafiaConfig.class, Paths.get("config.json")) {
            @Override
            public MafiaConfig getConfig() {
                return config;
            }

            @Override
            public MafiaConfig getDefaultConfig() {
                return new MafiaConfig();
            }

            @Override
            public void setConfig(MafiaConfig config) {
                Main.config = config;
            }
        };
        mafiaConfigurable.loadConfig();
        try {
            Class.forName("org.jline.terminal.Terminal");

            // JLine2 available
            commandHandler = new JLineCommandHandler();
            LogHelper.info("JLine2 terminal enabled");
        } catch (ClassNotFoundException ignored) {
            commandHandler = new StdCommandHandler(true);
            LogHelper.warning("JLine2 isn't in classpath, using std");
        }
        commandHandler.registerCommand("help", new HelpCommand(commandHandler));
        commandHandler.registerCommand("gc", new GCCommand());
        commandHandler.registerCommand("clear", new ClearCommand(commandHandler));
        commandHandler.registerCommand("stop", new StopCommand());
        commandHandler.registerCommand("nextState", new NextStateCommand());
        WebSocketService.registerResponses();
        server = new NettyServerHandler(config.netty);
        CommonHelper.newThread("Netty Server Socket Thread", false, server).start();
        commandHandler.run();
    }
}
