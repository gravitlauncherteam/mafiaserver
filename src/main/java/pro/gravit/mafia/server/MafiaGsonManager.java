package pro.gravit.mafia.server;

import com.google.gson.GsonBuilder;
import pro.gravit.launcher.managers.GsonManager;
import pro.gravit.launcher.request.JsonResultSerializeAdapter;
import pro.gravit.launcher.request.WebSocketEvent;
import pro.gravit.mafia.server.socket.WebSocketService;
import pro.gravit.mafia.server.socket.response.WebSocketServerResponse;
import pro.gravit.utils.UniversalJsonAdapter;

public class MafiaGsonManager extends GsonManager {
    @Override
    public void registerAdapters(GsonBuilder builder) {
        super.registerAdapters(builder);
        builder.registerTypeAdapter(WebSocketServerResponse.class, new UniversalJsonAdapter<>(WebSocketService.providers));
        builder.registerTypeAdapter(WebSocketEvent.class, new JsonResultSerializeAdapter());
    }
}
