package pro.gravit.mafia.server.socket.handlers;

import pro.gravit.launcher.ssl.LauncherKeyStore;
import pro.gravit.launcher.ssl.LauncherTrustManager;
import pro.gravit.mafia.server.socket.MafiaNettyServer;
import pro.gravit.mafia.server.socket.NettyConfig;
import pro.gravit.utils.helper.LogHelper;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Set;

public final class NettyServerHandler implements Runnable, AutoCloseable {
    private SSLServerSocketFactory ssf;

    public volatile boolean logConnections = Boolean.getBoolean("launcher.logConnections");

    public MafiaNettyServer nettyServer;

    // API
    private Set<Socket> sockets;

    private transient final NettyConfig config;

    public NettyServerHandler(NettyConfig config) {
        this.config = config;
    }

    @Override
    public void close() {
        //TODO: Close Impl
    }

    public SSLContext SSLContextInit() throws NoSuchAlgorithmException, UnrecoverableKeyException, KeyStoreException, KeyManagementException, IOException, CertificateException {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new LauncherTrustManager()
        };
        KeyStore ks = LauncherKeyStore.getKeyStore("keystore", "PSP1000");

        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
                .getDefaultAlgorithm());
        kmf.init(ks, "PSP1000".toCharArray());
        SSLContext sc = SSLContext.getInstance("TLSv1.2");
        sc.init(kmf.getKeyManagers(), trustAllCerts, new SecureRandom());
        return sc;
    }

    @Override
    public void run() {
        LogHelper.info("Starting netty server socket thread");
        nettyServer = new MafiaNettyServer(config);
        for (NettyConfig.NettyBindAddress address : config.binds) {
            nettyServer.bind(new InetSocketAddress(address.address, address.port));
        }
    }
}

