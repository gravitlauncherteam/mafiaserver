package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.request.WebSocketEvent;
import pro.gravit.mafia.server.instance.MafiaPlayer;

public class PlayerStateChangedEvent implements WebSocketEvent {
    public final String target;
    public final MafiaPlayer.PlayerState state;

    public PlayerStateChangedEvent(MafiaPlayer player) {
        this.target = player.username;
        this.state = player.state;
    }

    @Override
    public String getType() {
        return "playerStateChanged";
    }
}
