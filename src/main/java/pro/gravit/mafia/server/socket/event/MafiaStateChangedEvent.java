package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.request.WebSocketEvent;
import pro.gravit.mafia.server.instance.MafiaAction;
import pro.gravit.mafia.server.instance.MafiaPlayer;
import pro.gravit.mafia.server.instance.MafiaRole;
import pro.gravit.mafia.server.instance.MafiaState;

import java.util.List;

public class MafiaStateChangedEvent implements WebSocketEvent {
    public final MafiaState state;
    public static class PrivatePlayerInfo
    {
        public final MafiaRole role;
        public final String username;

        public PrivatePlayerInfo(MafiaPlayer player) {
            role = player.role;
            username = player.username;
        }
    }
    public List<PrivatePlayerInfo> playerInfo;
    public List<MafiaAction> events;
    public MafiaRole.MafiaTeam win;

    public MafiaStateChangedEvent(MafiaState state) {
        this.state = state;
    }

    @Override
    public String getType() {
        return "stateChanged";
    }
}
