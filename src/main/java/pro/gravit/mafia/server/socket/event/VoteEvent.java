package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.request.WebSocketEvent;

public class VoteEvent implements WebSocketEvent {
    public final String author;
    public final String target;
    public final boolean plus;

    public VoteEvent(String author, String target, boolean plus) {
        this.author = author;
        this.target = target;
        this.plus = plus;
    }

    @Override
    public String getType() {
        return "vote";
    }
}
