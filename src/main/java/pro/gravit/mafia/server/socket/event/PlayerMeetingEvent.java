package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.request.WebSocketEvent;
import pro.gravit.mafia.server.instance.MafiaPlayer;
import pro.gravit.mafia.server.instance.MafiaRole;

public class PlayerMeetingEvent implements WebSocketEvent {
    public final MafiaRole role;

    public PlayerMeetingEvent(MafiaPlayer player) {
        this.role = player.role;
    }

    @Override
    public String getType() {
        return "playerMeeting";
    }
}
