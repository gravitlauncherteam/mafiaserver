package pro.gravit.mafia.server.socket.response;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.mafia.server.instance.MafiaChatChannel;
import pro.gravit.mafia.server.socket.Client;

public class SendChatResponse extends SimpleResponse {
    public String message;
    public MafiaChatChannel chatChannel;
    @Override
    public String getType() {
        return "sendChat";
    }

    @Override
    public void execute(ChannelHandlerContext ctx, Client client) throws Exception {
        if(client.instance == null)
        {
            sendError("You not join to Mafia room");
            return;
        }
        if(message == null || message.isEmpty() || message.length() > 512)
        {
            sendError("Message length invalid");
            return;
        }
        if(chatChannel == null) chatChannel = client.player.chatChannel;
        if(!client.instance.sendChat(client.player, chatChannel, message))
        {
            sendError("Write in this chat channel not allowed");
        }
    }
}
