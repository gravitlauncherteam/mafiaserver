package pro.gravit.mafia.server.socket.response;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.mafia.server.instance.MafiaInstance;
import pro.gravit.mafia.server.socket.Client;

public class DialogResponse extends SimpleResponse {
    public long id;
    public String target;
    @Override
    public String getType() {
        return "dialog";
    }

    @Override
    public void execute(ChannelHandlerContext ctx, Client client) throws Exception {
        if(client.instance == null)
        {
            sendError("You not join to Mafia room");
            return;
        }
        MafiaInstance instance = client.instance;
        if(!instance.callDialog(client.player, id, target))
        {
            sendError("You not allow use this dialog");
        }
    }
}
