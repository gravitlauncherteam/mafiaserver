package pro.gravit.mafia.server.socket;

import pro.gravit.launcher.ClientPermissions;
import pro.gravit.mafia.server.instance.MafiaInstance;
import pro.gravit.mafia.server.instance.MafiaPlayer;
import pro.gravit.utils.helper.LogHelper;

public class Client {
    public long session;
    public long timestamp;
    public Type type;
    public boolean isAuth;
    public boolean checkSign;
    public ClientPermissions permissions;
    public String username;
    public MafiaInstance instance;
    public MafiaPlayer player;
    public transient LogHelper.OutputEnity logOutput;

    public Client(long session) {
        this.session = session;
        timestamp = System.currentTimeMillis();
        type = Type.USER;
        isAuth = false;
        permissions = ClientPermissions.DEFAULT;
        username = "";
        checkSign = false;
    }

    //Данные авторизации
    public void up() {
        timestamp = System.currentTimeMillis();
    }

    public enum Type {
        SERVER,
        USER
    }
}
