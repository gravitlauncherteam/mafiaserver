package pro.gravit.mafia.server.socket.response;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.mafia.server.socket.Client;
import pro.gravit.mafia.server.socket.event.PongEvent;

public class PingResponse extends SimpleResponse {
    @Override
    public String getType() {
        return "ping";
    }

    @Override
    public void execute(ChannelHandlerContext ctx, Client client) throws Exception {
        sendResult(new PongEvent());
    }
}
