package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.events.RequestEvent;
import pro.gravit.mafia.server.instance.MafiaPlayer;
import pro.gravit.mafia.server.instance.MafiaState;

import java.util.List;

public class JoinEvent extends RequestEvent {

    public static class PlayerPublicInfo {
        public final String username;

        public PlayerPublicInfo(MafiaPlayer player) {
            this.username = player.username;
        }
    }
    public final List<PlayerPublicInfo> players;
    public final MafiaState state;
    public JoinEvent(List<PlayerPublicInfo> players, MafiaState state) {
        this.players = players;
        this.state = state;
    }

    @Override
    public String getType() {
        return "join";
    }
}
