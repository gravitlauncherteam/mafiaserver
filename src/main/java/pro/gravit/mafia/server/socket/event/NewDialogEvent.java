package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.request.WebSocketEvent;
import pro.gravit.mafia.server.instance.AbstractDialog;

import java.util.List;

public class NewDialogEvent implements WebSocketEvent {
    public final String dialogType;
    public final List<String> targets;
    public final long id;

    public NewDialogEvent(AbstractDialog dialog) {
        this.dialogType = dialog.getType();
        this.id = dialog.id;
        this.targets = dialog.getTargets();
    }


    @Override
    public String getType() {
        return "newDialog";
    }
}
