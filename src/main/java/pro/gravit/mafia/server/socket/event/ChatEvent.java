package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.request.WebSocketEvent;
import pro.gravit.mafia.server.instance.MafiaChatChannel;

public class ChatEvent implements WebSocketEvent {
    public final String author;
    public final String message;
    public final MafiaChatChannel channel;

    public ChatEvent(String author, String message, MafiaChatChannel channel) {
        this.author = author;
        this.message = message;
        this.channel = channel;
    }

    @Override
    public String getType() {
        return "chat";
    }
}
