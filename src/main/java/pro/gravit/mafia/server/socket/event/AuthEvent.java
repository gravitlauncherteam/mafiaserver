package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.events.RequestEvent;

public class AuthEvent extends RequestEvent {
    @Override
    public String getType() {
        return "auth";
    }
}
