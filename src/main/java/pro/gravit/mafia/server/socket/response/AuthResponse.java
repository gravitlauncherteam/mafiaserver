package pro.gravit.mafia.server.socket.response;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.mafia.server.socket.Client;
import pro.gravit.mafia.server.socket.event.AuthEvent;

public class AuthResponse extends SimpleResponse {
    public String username;
    @Override
    public String getType() {
        return "auth";
    }

    @Override
    public void execute(ChannelHandlerContext ctx, Client client) throws Exception {
        if(username == null || username.isEmpty())
        {
            sendError("Invalid nickname");
            return;
        }
        client.username = username;
        client.isAuth = true;
        sendResult(new AuthEvent());
    }
}
