package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.request.WebSocketEvent;

public class PlayerConnectEvent implements WebSocketEvent {
    public final String username;

    public PlayerConnectEvent(String username) {
        this.username = username;
    }

    @Override
    public String getType() {
        return "playerConnect";
    }
}
