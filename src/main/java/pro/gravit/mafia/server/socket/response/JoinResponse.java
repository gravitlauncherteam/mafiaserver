package pro.gravit.mafia.server.socket.response;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.mafia.server.instance.MafiaInstance;
import pro.gravit.mafia.server.instance.MafiaService;
import pro.gravit.mafia.server.socket.Client;

public class JoinResponse extends SimpleResponse {
    public long id;
    @Override
    public String getType() {
        return "join";
    }

    @Override
    public void execute(ChannelHandlerContext ctx, Client client) throws Exception {
        if(!client.isAuth)
        {
            sendError("You not auth");
            return;
        }
        if(client.instance != null)
        {
            sendError("You already connected");
            return;
        }
        if(id >= 0 && id <= MafiaService.activeInstances.size())
        {
            MafiaInstance instance = MafiaService.activeInstances.get((int) id);
            if(!instance.connectPlayer(client, ctx))
            {
                sendError("You can't join this mafia instance");
            }
        }
        else
        {
            sendError("Invalid ID");
        }
    }
}
