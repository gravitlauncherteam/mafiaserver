package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.events.RequestEvent;

public class PongEvent extends RequestEvent {
    @Override
    public String getType() {
        return "pong";
    }
}
