package pro.gravit.mafia.server.socket.response;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.mafia.server.instance.MafiaInstance;
import pro.gravit.mafia.server.instance.MafiaService;
import pro.gravit.mafia.server.socket.Client;
import pro.gravit.mafia.server.socket.event.StartEvent;

public class StartResponse extends SimpleResponse {
    @Override
    public String getType() {
        return "start";
    }

    @Override
    public void execute(ChannelHandlerContext ctx, Client client) throws Exception {
        if(!client.isAuth)
        {
            sendError("You not auth");
            return;
        }
        if(client.instance != null)
        {
            sendError("You already connected");
            return;
        }
        MafiaInstance instance = new MafiaInstance();
        MafiaService.activeInstances.add(instance);
        instance.connectPlayer(client, ctx);
        sendResult(new StartEvent(MafiaService.activeInstances.size() - 1));
    }
}
