package pro.gravit.mafia.server.socket.event;

import pro.gravit.launcher.events.RequestEvent;

public class StartEvent extends RequestEvent {
    public final long id;

    public StartEvent(long id) {
        this.id = id;
    }

    @Override
    public String getType() {
        return "start";
    }
}
