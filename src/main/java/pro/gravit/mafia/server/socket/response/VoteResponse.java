package pro.gravit.mafia.server.socket.response;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.mafia.server.socket.Client;

public class VoteResponse extends SimpleResponse {
    public String target;
    @Override
    public String getType() {
        return "vote";
    }

    @Override
    public void execute(ChannelHandlerContext ctx, Client client) throws Exception {
        if(target == null || target.isEmpty()) return;
        if(client.instance == null)
        {
            sendError("You not join to Mafia room");
            return;
        }
        if(!client.instance.vote(client.player, target))
        {
            sendError("Vote not allowed");
        }
    }
}
