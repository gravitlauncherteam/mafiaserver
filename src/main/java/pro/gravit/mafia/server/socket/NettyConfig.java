package pro.gravit.mafia.server.socket;

import io.netty.handler.logging.LogLevel;

import java.util.HashMap;
import java.util.Map;

public class NettyConfig {
    public boolean ipForwarding;
    public NettyPerformanceConfig performance = new NettyPerformanceConfig();
    public NettyBindAddress[] binds = new NettyBindAddress[]{ new NettyBindAddress("localhost", 3232) };
    public LogLevel logLevel = LogLevel.DEBUG;

    public static class NettyPerformanceConfig {
        public boolean usingEpoll;
        public int bossThread;
        public int workerThread;
    }

    public static class NettyBindAddress {
        public String address;
        public int port;

        public NettyBindAddress(String address, int port) {
            this.address = address;
            this.port = port;
        }
    }
}
