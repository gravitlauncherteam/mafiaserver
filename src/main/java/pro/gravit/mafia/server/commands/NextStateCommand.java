package pro.gravit.mafia.server.commands;

import pro.gravit.mafia.server.instance.MafiaInstance;
import pro.gravit.mafia.server.instance.MafiaService;
import pro.gravit.mafia.server.instance.MafiaState;
import pro.gravit.utils.command.Command;

public class NextStateCommand extends Command {
    @Override
    public String getArgsDescription() {
        return "[id]";
    }

    @Override
    public String getUsageDescription() {
        return "next mafia state";
    }

    @Override
    public void invoke(String... args) throws Exception {
        verifyArgs(args, 1);
        int id = Integer.parseInt(args[0]);
        if(id < 0 || id > MafiaService.activeInstances.size()) throw new IllegalArgumentException("ID invalid");
        MafiaInstance instance = MafiaService.activeInstances.get(id);
        instance.nextState();
    }
}
