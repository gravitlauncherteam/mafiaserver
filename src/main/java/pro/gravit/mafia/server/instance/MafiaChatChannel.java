package pro.gravit.mafia.server.instance;

public enum MafiaChatChannel {
    OBSERVER,
    DAY,
    MAFIA
}
