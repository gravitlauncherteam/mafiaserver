package pro.gravit.mafia.server.instance;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.launcher.request.WebSocketEvent;
import pro.gravit.mafia.server.MafiaConfig;
import pro.gravit.mafia.server.Main;
import pro.gravit.mafia.server.instance.dialogs.WolfDialog;
import pro.gravit.mafia.server.socket.Client;
import pro.gravit.mafia.server.socket.WebSocketService;
import pro.gravit.mafia.server.socket.event.*;

import java.util.*;
import java.util.stream.Collectors;

public class MafiaInstance {
    public final List<MafiaPlayer> players = new ArrayList<>();
    public final WebSocketService service;
    public final MafiaConfig config;
    public MafiaState state = MafiaState.WAITING;
    public List<MafiaAction> actions = new ArrayList<>();
    public List<MafiaRole> roles = new ArrayList<>();
    public List<AbstractDialog> dialogs = new ArrayList<>();
    public Timer timer = new Timer();
    public int ticks;

    public MafiaInstance() {
        service = Main.server.nettyServer.service;
        config = Main.config;
    }

    public boolean connectPlayer(Client client, ChannelHandlerContext channel)
    {
        for(MafiaPlayer player : players)
        {
            if(player.username.equals(client.username)) return false;
        }
        MafiaPlayer newPlayer = new MafiaPlayer(this, client.username, channel);
        players.add(newPlayer);
        client.instance = this;
        client.player = newPlayer;
        JoinEvent event = new JoinEvent(
                players.stream().map(JoinEvent.PlayerPublicInfo::new).collect(Collectors.toList()),
                state);
        service.sendObject(channel, event);
        sendAllOtherPlayers(newPlayer, new PlayerConnectEvent(newPlayer.username));
        return true;
    }

    public void sendAllPlayers(WebSocketEvent event)
    {
        for(MafiaPlayer player : players)
        {
            service.sendObject(player.channel, event);
        }
    }

    public void sendAllOtherPlayers(MafiaPlayer ex_player, WebSocketEvent event)
    {
        for(MafiaPlayer player : players)
        {
            if(player.equals(ex_player)) continue;
            service.sendObject(player.channel, event);
        }
    }

    public boolean sendChat(MafiaPlayer author, MafiaChatChannel chatChannel, String message)
    {
        if(!isAllowWriteChatRoom(author, chatChannel)) return false;
        ChatEvent event = new ChatEvent(author.username, message, chatChannel);
        boolean result = false;
        for(MafiaPlayer player : players)
        {
            if(isAllowReadChatRoom(player, chatChannel))
            {
                service.sendObject(player.channel, event);
                result = true;
            }
        }
        return result;
    }
    public boolean isAllowWriteChatRoom(MafiaPlayer player, MafiaChatChannel channel)
    {
        if(!state.isAllow(channel)) return false;
        if(player.role.isAllow(channel)) return true;
        return player.chatChannel.equals(channel);
    }
    public boolean isAllowReadChatRoom(MafiaPlayer player, MafiaChatChannel channel)
    {
        if(!state.isAllow(channel)) return false;
        if(player.role.isAllow(channel)) return true;
        return player.chatChannel.equals(channel);
    }


    public void nextState()
    {
        if(!dialogs.isEmpty()) dialogs.clear();
        MafiaState newState = state;
        MafiaInstance instance = this;
        ticks++;
        int current_tick = ticks;
        switch (state)
        {
            case WAITING:
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        instance.nextState();
                    }
                }, 20*1000); // 20 second
                newState = MafiaState.MEETING;
                roles = getRoles();
                List<MafiaRole> playerRoles = new ArrayList<>(roles);
                Collections.shuffle(playerRoles);
                int it = 0;
                for (MafiaPlayer player : players) {
                    player.joinToGame(playerRoles.get(it));
                    it++;
                    if(it >= playerRoles.size()) it = 0;
                }
                break;
            case MEETING:
            case NIGHT:
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(current_tick == instance.ticks) instance.nextState();
                    }
                }, 5*60*1000); // 5 minutes
                newState = MafiaState.DAY;
                break;
            case DAY:
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if(current_tick == instance.ticks) instance.nextState();
                    }
                }, 3*60*1000); // 3 minutes
                //Votes
                MafiaPlayer exile = null;
                int max = 0;
                for(MafiaPlayer player : players)
                {
                    if(player.votes == 0) continue;
                    if(player.votes > max) {
                        exile = player;
                        max = player.votes;
                    }
                    else if(player.votes == max)
                        exile = null;
                    player.voteTo.clear();
                    player.votes = 0;
                }
                if(exile != null)
                {
                    actions.add(new MafiaAction(exile, MafiaAction.ActionType.EXILE));
                    exile.exit();
                }
                //
                newState = MafiaState.NIGHT;
                break;
        }
        for(MafiaAction action : actions)
        {
            if(action.actionType == MafiaAction.ActionType.KILL)
            {
                if(action.target != null && action.target.state == MafiaPlayer.PlayerState.ALIVE) action.target.exit();
            }
        }
        MafiaRole.MafiaTeam mayWinTeam = checkEnd();
        if(mayWinTeam != null) {
            newState = MafiaState.END;
        }
        state = newState;
        MafiaStateChangedEvent event = new MafiaStateChangedEvent(newState);
        event.events = actions;
        if(state.equals(MafiaState.NIGHT))
        {
            sendNightDialogs();
        }
        if(!dialogs.isEmpty())
        {
            dialogLink();
            execDialogs();
        }
        if(state.equals(MafiaState.END))
        {
            event.win = mayWinTeam;
            event.playerInfo = players.stream().map(MafiaStateChangedEvent.PrivatePlayerInfo::new).collect(Collectors.toList());
            for(MafiaPlayer pl : players)
            {
                pl.chatChannel = MafiaChatChannel.OBSERVER;
                pl.role = MafiaRole.OBSERVER;
            }
        }
        sendAllPlayers(event);
        actions.clear();
    }
    public List<MafiaRole> getRoles()
    {
        List<MafiaRole> result = new ArrayList<>();
        result.add(MafiaRole.WOLF);
        while(result.size() < players.size()) result.add(MafiaRole.TOWNIE);
        return result;
    }
    public MafiaRole.MafiaTeam checkEnd()
    {
        MafiaRole.MafiaTeam lastTeam = null;
        for(MafiaPlayer player : players)
        {
            if(player.role.team == null || player.state != MafiaPlayer.PlayerState.ALIVE) continue;
            if(lastTeam == null)
                lastTeam = player.role.team;
            else if (!lastTeam.equals(player.role.team)) {
                    return null;
            }
        }
        return lastTeam;
    }
    public MafiaPlayer getByName(String username)
    {
        for(MafiaPlayer player : players)
        {
            if(player.username.equals(username)) return player;
        }
        return null;
    }
    public boolean vote(MafiaPlayer player, String target)
    {
        MafiaPlayer targetPlayer = getByName(target);
        if(targetPlayer == null) return false;
        return vote(player, targetPlayer);
    }
    public boolean vote(MafiaPlayer player, MafiaPlayer target)
    {
        if(!state.equals(MafiaState.DAY)) return false;
        boolean plus;
        if(player.voteTo.contains(target))
        {
            player.voteTo.remove(target);
            target.votes--;
            plus = false;
        }
        else
        {
            player.voteTo.add(target);
            target.votes++;
            plus = true;
        }
        VoteEvent event = new VoteEvent(player.username, target.username, plus);
        sendAllPlayers(event);
        return true;
    }
    public boolean callDialog(MafiaPlayer player, long id, String target)
    {
        for(AbstractDialog dialog : dialogs)
        {
            if(dialog.id == id && dialog.player.equals(player) && !dialog.end)
            {
                dialog.action(target);
                return true;
            }
        }
        return false;
    }
    private void dialogLink()
    {
        for(AbstractDialog dialog : dialogs)
        {
            for(AbstractDialog dep : dialog.dependencies)
            {
                dep.dependent.add(dialog);
            }
        }
    }
    private void sendNightDialogs()
    {
        for(MafiaPlayer player : players)
        {
            if(player.state.equals(MafiaPlayer.PlayerState.ALIVE))
            {
                switch (player.role)
                {

                    case OBSERVER:
                    case TOWNIE:
                        break;
                    case WOLF:
                        dialogs.add(new WolfDialog(player, this));
                        break;
                }
            }
        }
    }
    private void execDialogs()
    {
        for(AbstractDialog dialog : dialogs)
        {
            if(dialog.isRunnable()) dialog.run();
        }
    }
}
