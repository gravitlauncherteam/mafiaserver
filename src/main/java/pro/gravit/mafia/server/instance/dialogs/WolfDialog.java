package pro.gravit.mafia.server.instance.dialogs;

import pro.gravit.mafia.server.instance.AbstractDialog;
import pro.gravit.mafia.server.instance.MafiaAction;
import pro.gravit.mafia.server.instance.MafiaInstance;
import pro.gravit.mafia.server.instance.MafiaPlayer;

import java.util.List;

public class WolfDialog extends AbstractDialog {
    public WolfDialog(MafiaPlayer player, MafiaInstance instance) {
        super(player, instance);
    }

    @Override
    public String getType() {
        return "wolf";
    }

    @Override
    protected void dialogAction(String target) {
        if(target == null) return;
        MafiaPlayer player = instance.getByName(target);
        if(player == null) return;
        if(player.state != MafiaPlayer.PlayerState.ALIVE) return;
        instance.actions.add(new MafiaAction(player, MafiaAction.ActionType.KILL));
    }

    @Override
    public List<String> getTargets() {
        return getOtherPlayers();
    }
}
