package pro.gravit.mafia.server.instance;

public enum MafiaState {
    WAITING(new MafiaChatChannel[]{MafiaChatChannel.OBSERVER}),
    MEETING(new MafiaChatChannel[]{MafiaChatChannel.MAFIA}),
    DAY(null),
    NIGHT(new MafiaChatChannel[]{MafiaChatChannel.MAFIA}),
    END(new MafiaChatChannel[]{MafiaChatChannel.OBSERVER});
    public final MafiaChatChannel[] allowChannels; //null - все

    MafiaState(MafiaChatChannel[] allowChannels) {
        this.allowChannels = allowChannels;
    }

    public boolean isAllow(MafiaChatChannel chatChannel)
    {
        if(allowChannels != null)
        {
            boolean allowed = false;
            for(MafiaChatChannel ch : allowChannels)
            {
                if(ch.equals(chatChannel))
                {
                    allowed = true;
                    break;
                }
            }
            return allowed;
        }
        return true;
    }
}
