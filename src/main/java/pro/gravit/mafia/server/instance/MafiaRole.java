package pro.gravit.mafia.server.instance;

public enum MafiaRole {
    OBSERVER(null, new MafiaChatChannel[]{MafiaChatChannel.OBSERVER}),
    WOLF(MafiaTeam.MAFIA, new MafiaChatChannel[]{MafiaChatChannel.DAY, MafiaChatChannel.MAFIA}),
    TOWNIE(MafiaTeam.TOWNIE, new MafiaChatChannel[]{MafiaChatChannel.DAY});
    public final MafiaChatChannel[] allowChannels;
    public final MafiaTeam team;
    public enum MafiaTeam
    {
        MAFIA,
        TOWNIE,
    }

    MafiaRole(MafiaTeam team, MafiaChatChannel[] allowChannels) {
        this.allowChannels = allowChannels;
        this.team = team;
    }
    public boolean isAllow(MafiaChatChannel chatChannel)
    {
        if(allowChannels != null)
        {
            boolean allowed = false;
            for(MafiaChatChannel ch : allowChannels)
            {
                if(ch.equals(chatChannel))
                {
                    allowed = true;
                    break;
                }
            }
            return allowed;
        }
        return true;
    }
}
