package pro.gravit.mafia.server.instance;

import pro.gravit.mafia.server.socket.event.NewDialogEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractDialog implements Runnable {
    public static long incrementID = 0;
    public final List<AbstractDialog> dependencies = new ArrayList<>();
    public final List<AbstractDialog> dependent = new ArrayList<>();
    public final long id;
    public final MafiaPlayer player;
    public final MafiaInstance instance;
    public boolean end = false;

    public AbstractDialog(MafiaPlayer player, MafiaInstance instance) {
        this.player = player;
        this.instance = instance;
        this.id = incrementID;
        incrementID++;
    }

    public boolean isRunnable()
    {
        return dependencies.isEmpty();
    }
    @Override
    public void run()
    {
        NewDialogEvent event = new NewDialogEvent(this);
        instance.service.sendObject(player.channel, event);
    }
    public void action(String target)
    {
        if(end) return;
        dialogAction(target);
        end = true;
        instance.dialogs.remove(this);
        for(AbstractDialog dialog : dependent)
        {
            dialog.dependencies.remove(this);
            if(dialog.isRunnable()) dialog.run();
        }
        if(instance.dialogs.size() == 0)
        {
            instance.nextState();
        }
    }

    protected List<String> getOtherPlayers()
    {
        return instance.players.stream().filter( (p) -> p.state ==  MafiaPlayer.PlayerState.ALIVE && p != player)
                .map( (p) -> p.username ).collect(Collectors.toList());
    }

    public abstract String getType();
    protected abstract void dialogAction(String target);
    public abstract List<String> getTargets();
}
