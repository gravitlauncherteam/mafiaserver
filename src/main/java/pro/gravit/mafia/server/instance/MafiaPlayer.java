package pro.gravit.mafia.server.instance;

import io.netty.channel.ChannelHandlerContext;
import pro.gravit.mafia.server.socket.event.PlayerMeetingEvent;
import pro.gravit.mafia.server.socket.event.PlayerStateChangedEvent;

import java.util.ArrayList;
import java.util.List;

public class MafiaPlayer {
    public final String username;
    public final ChannelHandlerContext channel;
    public final MafiaInstance instance;
    public MafiaChatChannel chatChannel = MafiaChatChannel.OBSERVER;
    public MafiaRole role = MafiaRole.OBSERVER;
    public PlayerState state = PlayerState.OBSERVER;
    //Vote params
    public List<MafiaPlayer> voteTo = new ArrayList<>();
    public int votes = 0;

    public MafiaPlayer(MafiaInstance instance, String username, ChannelHandlerContext channel) {
        this.instance = instance;
        this.username = username;
        this.channel = channel;
    }
    public enum PlayerState
    {
        OBSERVER,
        ALIVE
    };
    public void exit()
    {
        state = PlayerState.OBSERVER;
        chatChannel = MafiaChatChannel.OBSERVER;
        PlayerStateChangedEvent changedEvent = new PlayerStateChangedEvent(this);
        instance.sendAllPlayers(changedEvent);
    }
    public void joinToGame(MafiaRole role)
    {
        this.role = role;
        this.chatChannel = MafiaChatChannel.DAY;
        this.state = PlayerState.ALIVE;
        PlayerMeetingEvent event = new PlayerMeetingEvent(this);
        instance.service.sendObject(channel, event);
    }
}
