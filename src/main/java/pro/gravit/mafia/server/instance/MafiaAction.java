package pro.gravit.mafia.server.instance;

public class MafiaAction {
    public MafiaAction(MafiaPlayer target, ActionType actionType) {
        this.target = target;
        this.targetUsername = target.username;
        this.actionType = actionType;
    }

    public enum ActionType
    {
        KILL, EXILE
    }
    public transient final MafiaPlayer target;
    public final String targetUsername;
    public final ActionType actionType;
}
