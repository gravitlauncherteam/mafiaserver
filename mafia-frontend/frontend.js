console.log("Init frontend")
var frontend = {};
frontend.add_chat = function(channel, author, message) {
    $("#chat").append("<div><b>[</b><span class=\"chat_room\">"+channel+"</span><b>]</b> <span class=\"chat_author\">"+author+"</span>: <span class=\"chat_message\">"+message+"</span></div>");
}
frontend.players = [];
window.onload = function() {
    $("#chat_send").click(function() {
        api.chat($("#chat_input")[0].value);
        
        $("#chat_input")[0].value = "";
    });
    $("#chat_input").on('keypress',function(e) {
        if(e.which == 13) {
            api.chat($("#chat_input")[0].value);
            $("#chat_input")[0].value = "";
        }
    });
    api.message_event = function(event) {
        if(event.type == "chat")
        {
            frontend.add_chat(event.channel, event.author, event.message);
        }
        else if(event.type == "auth")
        {
            frontend.add_chat("SERVICE", "Server", "Вы успешно авторизировались");
        }
        else if(event.type == "join")
        {
            frontend.add_chat("SERVICE", "Server", "Вы вошли в комнату( "+event.players.length+" игроков)");
            $.each(event.players, function(index, value) {
                frontend.players.push(value.username);
            });
        }
        else if(event.type == "start")
        {
            frontend.add_chat("SERVICE", "Server", "Вы создали комнату номер "+event.id+"");
        }
        else if(event.type == "playerConnect")
        {
            frontend.add_chat("SERVICE", event.username, " Входит в игру");
            frontend.players.push(event.username);
        }
        else if(event.type == "stateChanged")
        {
            var text = event.state;
            if(event.state == "MEETING") text = "Добро пожаловать в игру! Знакомьтесь со своими ролями и продумывайте план действий";
            if(event.state == "DAY") text = "Наступает утро, все жители просыпаются";
            if(event.state == "NIGHT") text = "Наступает ночь, жители засыпают. Но есть и те, у кого на эту ночь свои планы";
            if(event.state == "END") text = "Игра закончена, победа команды "+event.win+". Поздравляем!";
            frontend.add_chat("SERVICE", "Server", text);
        }
        else if(event.type == "playerMeeting")
        {
            frontend.add_chat("SERVICE", "Неизвестный", "Вы "+event.role+". Удачи!");
        }
        else if(event.type == "playerStateChanged")
        {
            if(event.state == "OBSERVER") {
                frontend.add_chat("SERVICE", "Server", "Игрок "+event.target+" покидает нас");
                frontend.players = frontend.players.splice($.inArray(event.target, y), 1);
            }
        }
        else if(event.type == "vote")
        {
            if(event.plus == true)
                frontend.add_chat("SERVICE", event.author, "Я голосую против игрока "+event.target);
            else
                frontend.add_chat("SERVICE", event.author, "Я снимаю свой голос против игрока "+event.target);
        }
        else if(event.type == "newDialog")
        {
            var question = event.dialogType;
            if(event.dialogType == "wolf") question = "Кого вы хотите съеть этой ночью?";
            if(event.targets)
            {
                var id = event.id;
                frontend.setListModal(question, event.targets, function(e) {
                    api.dialog(id, e);
                }, function() {
                    api.dialog(id, null);
                });
            }
            else
            {
                var id = event.id;
                frontend.setListModal(question, event.targets, function(e) {
                    api.dialog(id, e);
                }, function() {
                    api.dialog(id, null);
                });
            }
        }
        else if(event.type == "error")
        {
            frontend.add_chat("ERROR", "Server", event.error);
        }
        
    }
    api.onopen = function(event) {
        frontend.add_chat("SERVICE", "WebSockets", "Соеденение успешно установлено");
        api.auth($('#authModalNickname')[0].value);
        $('#authModal').modal('hide');
    }
    $("#authModalButton").click(function() {
        api.connect($('#authModalUrl')[0].value);
    });
    $('#authModal').modal();
    
}
frontend.setTextModal = function(question, callback, cancel_callback) {
    var cb = callback;
    $('#textModalLabel').text(question);
    $('#textModalData')[0].value = "";
    $('#textModal').modal('show');
    $("#textModalButton").unbind("click");
    $("#textModalButton").click(function() {
        cb($('#textModalData')[0].value);
        $('#textModal').modal('hide');
    });
}
frontend.setListModal = function(question, list, callback, cancel_callback)
{
    var cb = callback;
    $('#listModalLabel').text(question);
    $('#listModalData').empty();
    $.each(list, function(index, val) {
        var o = new Option(val, val);
        /// jquerify the DOM object 'o' so we can use the html method
        $(o).html(val);
        $("#listModalData").append(o);
    });
    $('#listModal').modal('show');
    $("#listModalButton").unbind("click");
    $("#listModalButton").click(function() {
        cb($('#listModalData').val());
        $('#listModal').modal('hide');
    });
}
frontend.join = function() {
    frontend.setTextModal("Укажите номер комнаты", function(e) {
        api.join(e);
    }
    );
}
frontend.vote = function() {
    frontend.setListModal("Кого вы хотите выгнать?", frontend.players, function(e) {
        api.vote(e);
    });
}
