var api = {}
api.connect = function(url) {
        api.socket = new WebSocket("ws://localhost:3232/api");
        api.socket.onopen = api.onopen;
        api.socket.onclose = api.onclose;
        api.socket.onmessage = api.onmessage;
        api.socket.onerror = api.onerror;
};

api.onclose = function(event) {
  if (event.wasClean) {
    alert('Соединение закрыто чисто');
  } else {
    alert('Обрыв соединения'); // например, "убит" процесс сервера
  }
  alert('Код: ' + event.code + ' причина: ' + event.reason);
};

api.onmessage = function(event) {
   var obj = JSON.parse(event.data);
   console.log(obj);
   api.message_event(obj);
  //alert("Получены данные " + event.data);
};

api.onerror = function(error) {
  alert("Ошибка " + error.message);
};

api.send = function(obj) {
    console.log(obj);
    var json_obj = JSON.stringify(obj);
    api.socket.send(json_obj);
};


api.auth = function(username) {
    api.send({'type': 'auth', "username": username});
};
api.join = function(id) {
    api.send({'type': 'join', "id": id});
};
api.start = function() {
    api.send({'type': 'start'});
};
api.chat = function(message) {
    api.send({'type': 'sendChat', 'message': message});
};
api.chatCustom = function(channel, message) {
    api.send({'type': 'sendChat', 'chatChannel': channel, 'message': message});
};
api.vote = function(target) {
    api.send({'type': 'vote', 'target': target});
};
api.dialog = function(id, target) {
    api.send({'type': 'dialog', 'id': id, 'target': target});
};
